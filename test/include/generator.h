// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
//
//

#ifndef TEST_INCLUDE_GENERATOR_H_

// clang-format off
#include "generator/base.h"
#include "generator/vda5050.h"
#include "generator/base_impl.h"
#include "generator/vda5050_impl.h"
// clang-format on

#define TEST_INCLUDE_GENERATOR_H_

#endif  // TEST_INCLUDE_GENERATOR_H_
