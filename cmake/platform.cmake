# Copyright Open Logistics Foundation
# 
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#
# Populates the VDA5050_MESSAGE_STRUCTS_PLATFORM_DEFINITIONS list
# 

include(CheckCXXSymbolExists)

set(VDA5050_MESSAGE_STRUCTS_PLATFORM_DEFINITIONS "")

# Time conversions
check_cxx_symbol_exists(timegm ctime VDA5050_MESSAGE_STRUCTS_HAS_TIMEGM)
check_cxx_symbol_exists(gmtime_r ctime VDA5050_MESSAGE_STRUCTS_HAS_GMTIME_R)
check_cxx_symbol_exists(gmtime_s ctime VDA5050_MESSAGE_STRUCTS_HAS_GMTIME_S)
check_cxx_symbol_exists(_mkgmtime ctime VDA5050_MESSAGE_STRUCTS_HAS_MKGMTIME)

if (${VDA5050_MESSAGE_STRUCTS_HAS_TIMEGM})
  list(APPEND VDA5050_MESSAGE_STRUCTS_PLATFORM_DEFINITIONS "VDA5050_MESSAGE_STRUCTS_HAS_TIMEGM")
endif()
if (${VDA5050_MESSAGE_STRUCTS_HAS_GMTIME_R})
  list(APPEND VDA5050_MESSAGE_STRUCTS_PLATFORM_DEFINITIONS "VDA5050_MESSAGE_STRUCTS_HAS_GMTIME_R")
endif()
if (${VDA5050_MESSAGE_STRUCTS_HAS_GMTIME_S})
  list(APPEND VDA5050_MESSAGE_STRUCTS_PLATFORM_DEFINITIONS "VDA5050_MESSAGE_STRUCTS_HAS_GMTIME_S")
endif()
if (${VDA5050_MESSAGE_STRUCTS_HAS_MKGMTIME})
  list(APPEND VDA5050_MESSAGE_STRUCTS_PLATFORM_DEFINITIONS "VDA5050_MESSAGE_STRUCTS_HAS_MKGMTIME")
endif()

